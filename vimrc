set nocompatible

set number
set relativenumber
set colorcolumn=81
"set textwidth=80
set history=50
set ruler
set showcmd
set list
set listchars=tab:→\ ,eol:¬,trail:.,extends:>,precedes:<

" INDENT
set ts=2
set noautoindent
set backspace=indent,eol,start "backspace everything
syntax on
set t_Co=256

set encoding=utf-8
set t_Co=256

" SEARCH
set incsearch
set showmatch
set ignorecase
set smartcase
highlight search guibg=Grey
highlight search ctermbg=Grey

set diffopt+=iwhite

filetype plugin on
filetype indent on

" TAB-EXPAND
autocmd BufNewFile,BufRead *.c setlocal expandtab
autocmd BufNewFile,BufRead *.cpp setlocal expandtab
autocmd BufNewFile,BufRead *.h setlocal expandtab
autocmd BufNewFile,BufRead *.hpp setlocal expandtab
autocmd BufNewFile,BufRead *.py setlocal expandtab
autocmd BufNewFile,BufRead *.hs setlocal expandtab

" STATUS LINE
set laststatus=2

" REMAP
map ; :
nnoremap ;; ;

" COLOR
set termguicolors  "uncomment to use truecolor
colorscheme mu
hi colorcolumn ctermbg=11
